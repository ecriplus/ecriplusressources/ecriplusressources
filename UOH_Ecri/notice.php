<?php /* Template Name: notice */
get_header(); ?>

<?php
global $wpdb;

$uuid = $_GET['uuid']; // id unique de la notice en cours

$table_name = 'pagenotice'; // nom de la table de la bdd


$currentNotice = $wpdb->get_results(
    "SELECT * FROM {$table_name} WHERE uuid = '{$uuid}'"); // on récupère la notice actuelle

$currentNotice = $currentNotice[0]; // conversion en tableau
$ressources_link = "../index.php/ressource";


?>
    <div class="content-text">
        <div class="container major-container">

            <div class="resultats-filtres-courants">
            </div>
            <div class="resultats-recherche">
                <div class="container">
                    <a href="../index.php/ressource"> &lt; Retour aux résultats de recherche</a>
                    <br>
                    <br>
                    <h1 style="font-size: 1.5em;"><?php echo $currentNotice->titre; ?></h1>
                    <h2>Millet Guillaume - 2021</h2>
                    <br>
                    <div class="row">
                        <div class="col-lg-5 col-md-6">
                            <div class="carte-notice">
                                <a class="carte-notice-lien" href="https://www.canalc2.tv/video/15988" target="_blank">
                                </a><a class="carte-notice-lien" href="https://www.canalc2.tv/video/15988"
                                       target="_blank">
                                    <img class="carte-notice-image" style="box-shadow: 1px 1px 3px black;"
                                         src="https://uoh.fr/document/b6df8d78/86e9/4550/b6df8d78-86e9-4550-88fc-74e7996b7bfe.png"></br>
                                </a>
                                <br>
                                <div class="carte-notice-texte">
                                    <div class="carte-notice-liens-footer">
                                        <?php
                                        if (!empty($currentNotice->ressource_lien)) {
                                            $varUrl = json_decode($currentNotice->ressource_lien)->url;

                                            echo "<a class='btn btn-primary' title='Accéder à la ressource' target='_blank'
                                           href=" . $varUrl . "
                                           style='float: right; background-color: green;height:3em'><i class='fas fa-link'>Acceder
                                                a la ressource</i></a>";
                                        }
                                        ?>

                                    </div>
                                    <br>
                                </div>


                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div>
                                <h1 style="font-size: 1.3em;">Description :</h1>
                                <div class="carte-notice-description"
                                     title="L’ultra-endurance : est-ce réellement une discipline extrême ?">
                                    <p><?php echo $currentNotice->description; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6" style="text-align: center;padding-left: 5%">
                            <div>
                                <ul style="list-style: none;padding-left: 1%; background-color: white; box-shadow: 1px 1px 3px black; margin-bottom: 10%; padding-top:5%; padding-bottom:5%">
                                    <li style="font-weight: bold;">UNIVERSITÉ PRODUCTRICE</li>
                                    <br>
                                    <?php if ($currentNotice->etablissement_porteur != null) {
                                        $etab = (json_decode($currentNotice->etablissement_porteur))->libelle;


                                        echo "<li><a href='" . $ressources_link . "?etablissement_porteur=" . $etab . "'>";
                                        echo $etab . "
                                             </a></li>";
                                    } ?>


                                </ul>
                                <ul style="list-style: none;padding-left: 1%; background-color: white; box-shadow: 1px 1px 3px black; margin-bottom: 10%; padding-top:5%; padding-bottom:5%">
                                    <li style="font-weight: bold;">DISCIPLINES</li>
                                    <br>

                                    <?php foreach ((explode("-", json_decode($currentNotice->specialites)[0])) as $discipline) {
                                        echo "<li><a href='" . $ressources_link . "?specialites=" . $discipline . "'>" . $discipline . "</a></li>";
                                    }
                                    ?>

                                </ul>
                                <ul style="list-style: none;padding-left: 1%; background-color: white; box-shadow: 1px 1px 3px black; margin-bottom: 10%; padding-top:5%; padding-bottom:5%">
                                    <li style="font-weight: bold;">MOTS-CLEFS</li>
                                    <br>
                                    <?php foreach ((json_decode($currentNotice->mots_cles)) as $cles) {
                                        echo "<li><a href='" . $ressources_link . "?mots_cles=" . $cles . "'>" . $cles . "</a></li>";
                                    }
                                    ?>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="pedagogie">
                        <h1 style="font-size: 1.5em; background-color: white; margin-right: 89%; padding-top: 1%; padding-bottom: 1%; padding-left: 1%;">
                            Pédagogie</h1>
                        <ul style="padding-left: 1%; background-color: white; box-shadow: 1px 1px 3px black; margin-bottom: 5%">
                            <?php
                            if ($currentNotice->types_pedagogiques != null) {
                                $types_pedago = "";
                                echo "
                             <li style = 'font-weight: bold;list-style: none;' > Type pédagogique </li >
                            <li >" ?><?php
                                foreach (json_decode($currentNotice->types_pedagogiques) as $types) {
                                    $types_pedago = $types_pedago . $types . ', ';
                                }
                                echo substr($types_pedago, 0, -2);
                                echo "</li>
                            <br>";



                            }
                            ?>

                            <?php
                            if ($currentNotice->niveaux != null) {
                                $allNiveaux = "";
                                echo "
                             <li style = 'font-weight: bold;list-style: none;' > Niveaux </li >
                            <li >" ?><?php
                                foreach (json_decode($currentNotice->niveaux) as $niveaux) {
                                    $allNiveaux = $allNiveaux . $niveaux . ', ';
                                }
                                echo substr($allNiveaux, 0, -2);
                                echo "</li>
                            <br>";
                            }
                            ?>

                            <?php
                            if ($currentNotice->objectifs_pedagogiques != null) {
                                echo "
                             <li style = 'font-weight: bold;list-style: none;' > Objectifs pédagogiques </li >
                              
                            ";
                                echo "<ul>";
                                foreach (json_decode($currentNotice->objectifs_pedagogiques) as $obj) {
                                    echo "<li>" . $obj . "</li>";
                                }
                                echo "</ul> </li>
                            <br>";
                               

                            }
                            ?>


                            <?php
                            if ($currentNotice->activites_induites != null) {
                                $activiteInduite = "";
                                echo "
                             <li style = 'font-weight: bold;list-style: none;' > Activités induites </li >
                              
                            <li >" ?><?php
                                foreach (json_decode($currentNotice->activites_induites) as $activite) {
                                    $activiteInduite = $activiteInduite . $activite . ', ';
                                }
                                echo substr($activiteInduite, 0, -2);
                                echo "</li>
                            <br>";
                            }
                            ?>

                            <?php
                            if ($currentNotice->proposition_utilisation != null) {
                                echo "
                             <li style = 'font-weight: bold;list-style: none;' > Proposition d'utilisation </li >";

                                echo "<ul>";
                                foreach (json_decode($currentNotice->proposition_utilisation) as $obj) {
                                    echo "<li>" . $obj . "</li>";
                                }
                                echo "</ul> </li>
                            <br>";
                            }
                            ?>


                            <?php
                            if ($currentNotice->duree_apprentissage != null) {
                                echo "
                             <li style = 'font-weight: bold;list-style: none;' > Durée d'apprentissage </li >
                              
                            <li >"; ?><?php
                                echo substr($currentNotice->duree_apprentissage, 2);
                                echo "</li>
                            <br>";
                            }
                            ?>

                            <?php
                            if ($currentNotice->langues_utilisateur != null) {
                                $languesLearn = "";
                                echo "
                             <li style = 'font-weight: bold;list-style: none;' > Langue de l'apprenant </li >
                              
                            <li >" ?><?php
                                foreach (json_decode($currentNotice->langues_utilisateur) as $langue) {
                                    $languesLearn = $languesLearn . $langue . ', ';
                                }
                                echo substr($languesLearn, 0, -2);
                                echo "</li>
                            <br>";
                            }
                            ?>

                        </ul>

                    </div>
                    <hr>
                    <br>
                    <p>Recommandations</p>
                    <br>
                </div>
            </div>

        </div>
    </div>
    </div>
    </div>


<?php get_footer() ?>
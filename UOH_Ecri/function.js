function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

function searchBarFunction() {
    var input, filter
    input = document.getElementById("search_bar");
    filter = input.value.toLowerCase();
    location.href = "?mots_cles="+filter;
}

function putFilter(value){
    var input
    input = document.getElementById("search_bar");
    input.value = value ;

}
<?php

//Titre de page dynamique
function theme_supports(){
    add_theme_support('title-tag');

}
//Enregistrement et utilisation des assets
function theme_register_assets(){
    wp_register_script( 'tailwinds', 'https://cdn.tailwindcss.com');
    wp_enqueue_script('tailwinds');
    wp_register_style( 'bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap');
}

function buildUrl($string,$actual_link,$filtre,$value) {
    $query = $string;
    $query[$filtre] = $value;
    $query_result = http_build_query($query);
    $returnLink = $actual_link."?".$query_result;

    return $returnLink ;
}

function buildVignette($string) {
    $explodedString  = explode('-',$string) ;
    $completeVignette = '';
    for($i = 0; $i < 3; $i+=1) {
        $completeVignette=$completeVignette.$explodedString[$i].'/';
    }
    return 'https://uoh.fr/document/'.$completeVignette.$string;
}

add_action('after_setup_theme', 'theme_supports') ;
add_action('wp_enqueue_scripts', 'theme_register_assets') ;


?>
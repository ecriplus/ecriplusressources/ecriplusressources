<?php /* Template Name: ressource */
get_header(); ?>
    <script>
        <?php require_once("function.js");?>
    </script>

<?php
global $wpdb;

$table_name = 'pagenotice';// nom de la table avec les notices


$actual_link = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "/?"; // lien actuel
$link_for_notice = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']; // lien pour la notice
$link_no_parameters = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH); // Lien sans les filtres


if (!isset($_GET['pages'])) {
    $page2 = 1; // S'il n'y a pas de filtre pour la page alors on est à la page 1
} else {
    $page2 = $_GET['pages']; // on récupère le numéro de page actuelle

}

$page1 = $page2 - 1;

$page3 = $page2 + 1;

$fullString = "SELECT * FROM $table_name WHERE";
$countString = "SELECT COUNT(*) FROM $table_name WHERE";
foreach ($_GET as $key => $value) {
    if ($key != 'pages' && $value != '') {
        $actual_link = $actual_link . $key . "=" . $value . "&";
        $value = ltrim($value);
        $valueAll = '\'%' . $value . '%\'';
        $fullString = $fullString . " " . $key . " LIKE " . $valueAll;
        $countString = $countString . " " . $key . " LIKE" . $valueAll;
        $fullString = $fullString . " AND ";
        $countString = $countString . " AND ";
    }
}


$fullString = substr($fullString, 0, -5);
$countString = substr($countString, 0, -5);


$nbRessources = $wpdb->get_var(
    "$countString");
$pageStart = ($page2 - 1) * 9;
$pageStop = ($pageStart + 1) * 9;
$fullString = $fullString . " ORDER BY date_publication DESC LIMIT {$pageStart},{$pageStop}";
$notice = $wpdb->get_results(
    "$fullString");


if (isset($_GET['mots_cles'])) { // Remplissement du champs de recherche
    ?>
    <script>
        var input
        input = document.getElementById("search_bar");
        input.value = <?php echo $_GET['mots_cles'] ?> ;

    </script>
    <?php
}

$lastPage = ceil($nbRessources / 9);

?>
    <div class="search_wrap search_wrap_3">
        <div class="search_box">
            <label>
                <input type="text" id="search_bar" class="input" placeholder="Rechercher parmi les ressources"
                       value=<?php if (isset($_GET['mots_cles'])) {
                    echo $_GET['mots_cles'];
                } ?>>
            </label>
            <div class="btn btn_common" id="button_search_bar" onclick="searchBarFunction()">
                <i class="fa fa-search" style="font-size:24px"></i>
            </div>
        </div>
    </div>

    <div class='content-text'>
        <div class='container major-container'>
            <div class='row'>
                <div class='col-lg-3 col-md-12'>
                    <div style='background-color: #dedede;' class='arbre-recherche'>
                        <a class='tree-title btn' data-toggle='collapse' href='#estampillage-tree' role='button'
                           aria-expanded='true' aria-controls='estampillage-tree'>
                            <div>
                                <div class='align-left'>
                                <span class='tree-root'>
                                    FILTRE
                                </span>
                                </div>
                                <div class='align-right'><i class='fas fa-sort-up'></i></div>
                                <div class='clearFloat'></div>
                            </div>
                        </a>

                        <hr>

                        <a class='tree-title btn' data-toggle='collapse' href='#discipline-tree' role='button'
                           aria-expanded='true' aria-controls='discipline-tree'>
                            <div>
                                <div class='align-left'>
                                <span class='tree-root'>
                                    Competences écrites
                                </span>
                                </div>
                                <div class='clearFloat'></div>
                            </div>

                        </a>
                        <div id='discipline-tree'
                             class='collapse show jstree jstree-2 jstree-default jstree-checkbox-selection' role='tree'
                             aria-multiselectable='true' tabindex='0' aria-activedescendant='j2_1' aria-busy='false'>
                            <ul class='jstree-container-ul jstree-children jstree-no-dots jstree-no-icons' role='group'>
                                <?php echo "
                                <li><a href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'mots_cles', 'orthographique') . ">Compétence orthographique</a></li>
                                <li><a href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'mots_cles', 'lexicale') . ">Compétences lexicales</li>
                                <li><a href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'mots_cles', 'syntaxique') . ">Compétences syntaxiques</li>
                                <li><a href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'mots_cles', 'discursive') . ">Compétences discursives</li>
                                <li><a href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'mots_cles', 'rhétorique') . ">Compétences rhétoriques et argumentatives</li>";
                                ?>
                            </ul>
                        </div>
                        <hr>


                        <a class='tree-title btn' data-toggle='collapse' href='#niveau-tree' role='button'
                           aria-expanded='true' aria-controls='niveau-tree'>
                            <div>
                                <div class='align-left'>
                                <span class='tree-root'>
                                    Thèmes
                                </span>
                                </div>
                                <div class='clearFloat'></div>
                            </div>
                        </a>
                        <div id='niveau-tree'
                             class='collapse show jstree jstree-3 jstree-default jstree-checkbox-selection' role='tree'
                             aria-multiselectable='true' tabindex='0' aria-activedescendant='j3_3' aria-busy='false'>
                            <ul class='jstree-container-ul jstree-children jstree-no-dots jstree-no-icons' role='group'>
                                <li>Accords grammaticaux</li>
                                <li>Construction de la phrase</li>
                                <li>Énonciation</li>
                                <li>Grammaire de texte</li>
                                <li>Orthographe</li>
                                <li>Ponctuation</li>
                                <li>Vocabulaire et expressions</li>
                            </ul>
                        </div>
                        <hr>


                        <a class='tree-title btn' data-toggle='collapse' href='#type-ressource-tree' role='button'
                           aria-expanded='false' aria-controls='type-ressource-tree'>
                            <div>
                                <div class='align-left'>
                                <span class='tree-root'>
                                    Ressources didactiques
                                </span>
                                </div>
                                <div class='clearFloat'></div>
                            </div>
                        </a>
                        <div id='type-ressource-tree'
                             class='collapse jstree jstree-4 jstree-default jstree-checkbox-selection' role='tree'
                             aria-multiselectable='true' tabindex='0' aria-activedescendant='j4_1' aria-busy='false'>
                            <ul class='jstree-container-ul jstree-children jstree-no-dots jstree-no-icons' role='group'>
                                <li>Questionnaires d'auto-évaluation</li>
                                <li>Port folio</li>
                                <li>APC</li>

                            </ul>
                        </div>
                        <hr>


                        <div class="dropdown">
                            <button onclick="myFunction()" class="dropbtn">Type de ressource</button>
                            <?php
                            echo "
                    <div id='myDropdown' class='dropdown-content'>       
                        <a href='" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'types_pedagogiques', 'MOOC') . "'>MOOC</a>
                        <a href='" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'types_pedagogiques', 'Exercices intéractifs') . "'>Exercices intéractifs</a>
                        <a href='" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'types_pedagogiques', 'Exercices pdf') . "'>Exercices pdf</a>
                        <a href='" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'types_pedagogiques', 'Diaporamas sonorisés') . "'>Diaporamas sonorisés</a>
                        <a href='" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'types_pedagogiques', 'Fiches et tableaux') . "'>Fiches et tableaux</a>
                        <a href='" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'types_pedagogiques', 'Vidéos') . "'>Vidéos</a>
                    </div> "; ?>
                        </div>
                    </div>
                </div>


                <div class='col-lg-9 col-md-12'>
                    <div class='top-resultats'>
                        <div class='container'>
                            <div class='row'>
                                <div class='col-md-6'>
                                    <div class='nb-resultats-query'>
                                        <span class='nb-resultats'><?php echo $nbRessources ?>   résultats </span>
                                    </div>
                                </div>
                                <div class='col-md-6'>
                                    <div class='filtres-resultats'>
                                        <div class='container'>
                                            <div class='row justify-content-right'>
                                                <div class='col-auto trier-par'>
                                                    Tri décroissant par
                                                </div>
                                                <div class='col-auto'>
                                                    <div class='dropdown'>
                                                        <button class='btn btn-default dropdown-toggle' type='button'
                                                                data-toggle='dropdown'>
                                                            Date de publication
                                                            <span class='caret'></span>
                                                        </button>
                                                        <ul class='dropdown-menu'>
                                                            <li>
                                                                <a class='link-tri' data-newtri='score'>
                                                                    Pertinence
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class='link-tri' data-newtri='date_publication'>
                                                                    Date de publication
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='resultats-filtres-courants'>
                    </div>
                    <div class='resultats-recherche'>
                        <div class='container'>


                            <!------ LES CARTES NOTICES ( 9 )-------------------------------------------------------------------------------------- -->

                            <div class='row'>
                                <?php
                                for ($i = 0; $i < 9; $i += 1) {
                                    if (!empty($notice[$i])) {
                                        if (!empty($notice[$i]->ressource_lien)) {
                                            $var = json_decode($notice[$i]->ressource_lien);
                                            $varString = $var->url;
                                        }

                                        echo

                                        "<div class='col-lg-4 col-md-6'>
                                        <div class='carte-notice'>
                                        ";
                                        if (isset($varString)) {
                                            echo " <a class='carte-notice-lien' href='" . $varString . "' target='_blank'>";
                                        } else {
                                            echo " <a class='carte-notice-lien' href='' target='_blank'>";
                                        }
                                        if ($notice[$i]->vignette != null) {
                                            echo "
                                                <img class='carte-notice-image' style='height:200px;width:100%;'
                                                     src='" . buildVignette($notice[$i]->vignette) . "'>";
                                        } else {
                                            echo "<img class='carte-notice-image' style='height:200px;width:100%;'
                                                     src='https://uoh.fr/front/wp-content/themes/uoh/assets_dist/prod/images/default_image.png'>";
                                        }
                                        echo "



                                            </a>
                                            <div class='carte-notice-texte'>
                                                <a class='carte-notice-lien' href='https://www.canalc2.tv/video/15988'
                                                   target='_blank'>
                                                    <h5 title='" .

                                            $notice[$i]->titre . "'>" . $notice[$i]->titre . "</h5>
                                                    <div class='carte-notice-universite' title='Université de Strasbourg'>";


                                        if (!empty($notice[$i]->etablissement_porteur)) {
                                            $var = json_decode($notice[$i]->etablissement_porteur);
                                            echo $var->libelle;
                                        }
                                        echo "
                                                    </div>
                                                </a>
                                                <div class='carte-notice-description'
                                                     title='L’ultra-endurance : est-ce réellement une discipline extrême ?'>
                                                    <p>" . $notice[$i]->description . "</p>
                                                </div>
                                            </div>
    
                                            <div class='carte-notice-footer'>
                                                <div class='carte-notice-universite-footer'>
                                                    <span class='text-Ecri+'>Ecri+</span>
                                                </div>
                                                <div class='carte-notice-liens-footer'>";


                                        if (!empty($notice[$i]->ressource_lien)) {
                                            echo "<a class='btn btn-primary' title='Accéder à la ressource'
                                                       target='_blank' href=" . $varString . "><i
                                                                class='fa fa-link'></i></a>
                                                    ";
                                        }
                                        echo " <a class='btn btn-primary' title='Accéder au descriptif'
                                                       href=" . $link_for_notice . "/notice?uuid=" . $notice[$i]->uuid . "><i
                                                                class='material-icons'>add</i></a>";
                                        echo "
    
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div> ";
                                    }
                                } ?>

                                <!-- FIN CARTES NOTICES -------------------------------------------------------------------------------------- -->


                                <!----PAGINATION ---------------------------------------------------------------------------------------------------->

                                <?php
                                if ($nbRessources > 0) {
                                    if (!isset($_GET['pages']) || $_GET['pages'] <= 1) {

                                        echo "
                    <div class='resultats-pagination'>
                        <div class='row justify-content-center'>
                            <div class='col-auto'>
                                <a class='btn btn-primary resultats-pagination-box-colored link-pagination' data-newpage='1' >&lt;&lt;</a>
                                <a class='btn btn-primary resultats-pagination-box-colored link-pagination' data-newpage='1' >&lt;</a>
                            </div>
          
                            <div class='col-auto'>
                                <a class='btn btn-primary resultats-pagination-box-colored link-pagination' data-newpage='1' 
                                        href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'pages', '1') . ">1</a>";
                                        if ($lastPage >= 2) {
                                            echo "
                                <a class='btn btn-primary resultats-pagination-box link-pagination' data-newpage='2'
                                        href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'pages', '2') . ">2</a>";
                                        }
                                        if ($lastPage >= 3) {
                                            echo "
                                
                                <a class='btn btn-primary resultats-pagination-box link-pagination' data-newpage='3'
                                                         href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'pages', '3') . ">3</a>";
                                        }
                                        echo "
                            </div>
                            <div class='col-auto'>";
                                        if ($page2 < $lastPage) {
                                            echo "
                                <a class='btn btn-primary resultats-pagination-box-colored link-pagination' data-newpage='2' href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'pages', $page3) . ">&gt;</a>
                                <a class='btn btn-primary resultats-pagination-box-colored link-pagination' data-newpage='229' href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'pages', $lastPage) . ">&gt;&gt;</a>";
                                        }
                                        echo "
                                
                            </div>
                        </div>
                    </div>";
                                    } else {
                                        echo "
                    <div class='resultats-pagination'>
                        <div class='row justify-content-center'>
                            <div class='col-auto'>
                                <a class='btn btn-primary resultats-pagination-box-colored link-pagination' data-newpage='1' href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'pages', '1') . ">&lt;&lt;</a>
                                <a class='btn btn-primary resultats-pagination-box-colored link-pagination' data-newpage='1' href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'pages', $page1) . ">&lt;</a>
                            </div>
          
                            <div class='col-auto'>
                                <a class='btn btn-primary resultats-pagination-box-colored link-pagination' data-newpage='1' 
                                        href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'pages', $page1) . ">$page1</a>";


                                        echo "
                                <a class='btn btn-primary resultats-pagination-box link-pagination' data-newpage='2'
                                        href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'pages', $page2) . ">$page2</a> ";

                                        if ($page3 <= $lastPage) {
                                            echo "
                                <a class='btn btn-primary resultats-pagination-box link-pagination' data-newpage = '3'
                                                         href = " . buildUrl($_GET, $_SERVER['PHP_SELF'], 'pages', $page3) . ">$page3</a>";
                                        }

                                        echo "
                            </div>
                            <div class='col-auto'>";
                                        if ($page2 < $lastPage) {
                                            echo "
                            <a class='btn btn-primary resultats-pagination-box-colored link-pagination' data-newpage='2' href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'pages', $page3) . ">&gt;</a>
                                <a class='btn btn-primary resultats-pagination-box-colored link-pagination' data-newpage='229' href=" . buildUrl($_GET, $_SERVER['PHP_SELF'], 'pages', $lastPage) . ">&gt;&gt;</a>";
                                        }
                                        echo "   
                                
                            </div>
                        </div>
                    </div>";
                                    }

                                }
                                ?>


                                <!---- FIN PAGINATION ---------------------------------------------------------------------------------------------------->


                            </div>
                        </div>
                    </div>
                </div>
            </div>


<?php get_footer() ?>
<?php get_header(); ?>
<?php
global $wpdb;

$table_name = 'pagenotice';// nom de la table avec les notices


$actual_link = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "/?"; // lien actuel
$link_for_notice = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']; // lien pour la notice
$link_no_parameters = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);


$filtre = ""; // filtre actif
$specialite = "";
$univ = "";


if (!isset($_GET['page'])) {
    $page2 = 1; // on récupère le numéro de page actuelle
} else {
    $page2 = $_GET['page'];
}

$page1 = $page2 - 1;

$page3 = $page2 + 1;

$fullString = "SELECT * FROM $table_name WHERE";
$countString = "SELECT COUNT(*) FROM $table_name WHERE";
foreach ($_GET as $key => $value) {
    if ($key != 'page' && $value != '') {
        $actual_link = $actual_link . $key . "=" . $value . "&";
        $value = ltrim($value);
        $valueAll = ''%' . $value . '%'';
        $fullString = $fullString . " " . $key . " LIKE " . $valueAll;
        $countString = $countString . " " . $key . " LIKE" . $valueAll;
        $fullString = $fullString . " AND ";
        $countString = $countString . " AND ";
    }
}


$fullString = substr($fullString, 0, -5);
$countString = substr($countString, 0, -5);


$nbRessources = $wpdb->get_var(
    "$countString");
$pageStart = ($page2 - 1) * 9;
$pageStop = ($pageStart + 1) * 9;
$fullString = $fullString . " ORDER BY date_publication DESC LIMIT {$pageStart},{$pageStop}";
$notice = $wpdb->get_results(
    "$fullString");
?>
    <div class="content-text">
        <div class="container major-container">

            <div class="resultats-filtres-courants">
            </div>
            <div class="resultats-recherche">
                <div class="container">
                    <div class='row' style="border:solid; border-color:grey">
                        <img src="<?php bloginfo('template_directory'); ?>/image/logoEcriplus.png"
                             alt="logo ecriplus" style="width:300px;height:auto;">
                        <br>
                        <br>
                        <h2>Amélioration en francais - Espace formation</h2>
                    </div>
                    <br>
                    <br>
                    <div class='row' style="border:solid ;margin-right: 70%; border-color:grey">
                        <p>Des ressoucres pour l'enseignement et l'apprentissage  <br>
                            du francais écrit à l'université
                        </p>
                    </div>
                    <br>
                    <a class='btn btn-primary' title='Accéder au ressource' target='_blank'
                       href="index.php/ressource" style='padding-top: 2%; padding-bottom: 2%;'><i class='fas fa-link'>Explorer les ressources</i></a>

                    <br>
                    <br>
                    <br>
                    <div class="row" style="border:solid ;border-color:khaki">
                        <p>Sur ce site, les enseignants, les tuteurs, les étudiants trouveront des ressources pour l'amélioration du français écrit au niveau universitaire</p>
                        <br>
                        <br>

                        <div class='resultats-recherche'>
                            <div class='container'>


                                <!------ LES CARTES NOTICES ( 9 )-------------------------------------------------------------------------------------- -->

                                <div class='row'>

                                    <?php
                                    for ($i = 0; $i < 6; $i += 1) {
                                        if($i == 0) {
                                           echo" <div class='col-lg-3 col-md-6'>
                                        <h2 style='font-size:xx-large'>Actualité</h2>
                                        <br>
                                        <div class= 'actualité'>
                                            <p>actu du jour ect</p>
                                        </div>
                                    </div>";
                                        }
                                        else if($i == 3) {
                                          echo  "<div class='col-lg-3 col-md-6'>
                                      
                                    </div>";
                                        }

                                        if (!empty($notice[$i])) {
                                            if (!empty($notice[$i]->ressource_lien)) {
                                                $var = json_decode($notice[$i]->ressource_lien);
                                                $varString = $var->url;
                                            }

                                            echo

                                            "<div class='col-lg-3 col-md-6'>
                                        <div class='carte-notice'>
                                        ";
                                            if (isset($varString)) {
                                                echo " <a class='carte-notice-lien' href='" . $varString . "' target='_blank'>";
                                            } else {
                                                echo " <a class='carte-notice-lien' href='' target='_blank'>";
                                            }
                                            if ($notice[$i]->vignette != null) {
                                                echo "
                                                <img class='carte-notice-image' style='height:200px;width:100%;'
                                                     src='" . buildVignette($notice[$i]->vignette) . "'>";
                                            } else {
                                                echo "<img class='carte-notice-image' style='height:200px;width:100%;'
                                                     src='https://uoh.fr/front/wp-content/themes/uoh/assets_dist/prod/images/default_image.png'>";
                                            }
                                            echo "



                                            </a>
                                            <div class='carte-notice-texte'>
                                                <a class='carte-notice-lien' href='https://www.canalc2.tv/video/15988'
                                                   target='_blank'>
                                                    <h5 title='" .

                                                $notice[$i]->titre . "'>" . $notice[$i]->titre . "</h5>
                                                    <div class='carte-notice-universite' title='Université de Strasbourg'>";


                                            if (!empty($notice[$i]->etablissement_porteur)) {
                                                $var = json_decode($notice[$i]->etablissement_porteur);
                                                echo $var->libelle;
                                            }
                                            echo "
                                                    </div>
                                                </a>
                                                <div class='carte-notice-description'
                                                     title='L’ultra-endurance : est-ce réellement une discipline extrême ?'>
                                                    <p>" . $notice[$i]->description . "</p>
                                                </div>
                                            </div>
    
                                            <div class='carte-notice-footer'>
                                                <div class='carte-notice-universite-footer'>
                                                    <span class='text-Ecri+'>Ecri+</span>
                                                </div>
                                                <div class='carte-notice-liens-footer'>";


                                            if (!empty($notice[$i]->ressource_lien)) {
                                                echo "<a class='btn btn-primary' title='Accéder à la ressource'
                                                       target='_blank' href=" . $varString . "><i
                                                                class='fa fa-link'></i></a>
                                                    ";
                                            }
                                            echo " <a class='btn btn-primary' title='Accéder au descriptif'
                                                       href=" . $link_for_notice . "/notice?uuid=" . $notice[$i]->uuid . "><i
                                                                class='material-icons'>add</i></a>";
                                            echo "
    
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div> ";
                                        }
                                    } ?>

                                    <!-- FIN CARTES NOTICES -------------------------------------------------------------------------------------- -->


                                    <!----PAGINATION ---------------------------------------------------------------------------------------------------->



                        <br>
                        <hr>
                        <br>
                        <p>Recommandations</p>
                        <br>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>


<?php get_footer() ?>
<?php


$searchedTerm = "ecri+"; // Terme à rechercher

$url = "https://unit-4.criann.fr/solr/notices/select?q=".$searchedTerm."'&fl=uuid&omitHeader=true&rows=50000&wt=json"; // Url du répertoire solr


$arrContextOptions=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
); // On gère les problèmes de certificat ssl

$actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

$response = file_get_contents($url, false, stream_context_create($arrContextOptions));
$reponseDecode = json_decode($response,true);
$reponseDecode = $reponseDecode['response']['docs'];
$table_name = "pagenotice";
global $wpdb;

foreach ($reponseDecode as $val) {
    $uuid = $val['uuid'];
    $urlUuid = "https://unit-4.criann.fr/solr/notices/select?q=".$searchedTerm."&wt=json&omitHeader=true&fq=uuid:" . $uuid;

    $reponsePerUuid = file_get_contents($urlUuid, false, stream_context_create($arrContextOptions));
    $jsonPerUuid = json_decode($reponsePerUuid,true);
    $jsonPerUuid = $jsonPerUuid['response']['docs'][0]; // on récupère le tableau en rapport avec les notices

    $exists = $wpdb->get_var(
        "SELECT date_modification FROM {$table_name} WHERE uuid = '{$uuid}'"
    );

    if ( ! $exists ) { // stock dans la base de donnée
        $wpdb->insert($table_name, array('uuid' => $uuid, 'domaines' => $jsonPerUuid['domaines'], 'date_publication'=>$jsonPerUuid['date_publication'],
            'date_creation'=>$jsonPerUuid['date_creation'],'date_modification'=>$jsonPerUuid['date_modification'], 'titre'=>$jsonPerUuid['titre'],
            'description'=>$jsonPerUuid['description'], 'ressource_lien'=>$jsonPerUuid['ressource_lien'], 'url'=>$jsonPerUuid['url'],
            'fichiers_attaches'=>$jsonPerUuid['fichiers_attaches'], 'etablissement_porteur'=>$jsonPerUuid['etablissement_porteur'],
            'niveaux'=>$jsonPerUuid['niveaux'],'objectifs_pedagogiques'=>$jsonPerUuid['objectifs_pedagogiques'],
            'types_pedagogiques'=>$jsonPerUuid['types_pedagogiques'],'activites_induites'=>$jsonPerUuid['activites_induites'],
            'langues_utilisateur'=>$jsonPerUuid['langues_utilisateur'], 'proposition_utilisation'=>$jsonPerUuid['proposition_utilisation'],
            'duree_apprentissage'=>$jsonPerUuid['duree_apprentissage'], 'langue_ressource'=>$jsonPerUuid['langues_ressource'],
            'mots_cles'=>$jsonPerUuid['mots_cles'],'association_associates'=>$jsonPerUuid['association_associates'],'specialites'=>$jsonPerUuid['specialites']
            ,'vignette'=>$jsonPerUuid['vignette']));
    }

    elseif($exists<$jsonPerUuid['date_modification']) { // on remplace dans la base de donnée si modif récente
        $wpdb->replace($table_name, array('uuid' => $uuid, 'domaines' => $jsonPerUuid['domaines'], 'date_publication'=>$jsonPerUuid['date_publication'],
            'date_creation'=>$jsonPerUuid['date_creation'],'date_modification'=>$jsonPerUuid['date_modification'], 'titre'=>$jsonPerUuid['titre'],
            'description'=>$jsonPerUuid['description'], 'ressource_lien'=>$jsonPerUuid['ressource_lien'], 'url'=>$jsonPerUuid['url'],
            'fichiers_attaches'=>$jsonPerUuid['fichiers_attaches'], 'etablissement_porteur'=>$jsonPerUuid['etablissement_porteur'],
            'niveaux'=>$jsonPerUuid['niveaux'],'objectifs_pedagogiques'=>$jsonPerUuid['objectifs_pedagogiques'],
            'types_pedagogiques'=>$jsonPerUuid['types_pedagogiques'],'activites_induites'=>$jsonPerUuid['activites_induites'],
            'langues_utilisateur'=>$jsonPerUuid['langues_utilisateur'], 'proposition_utilisation'=>$jsonPerUuid['proposition_utilisation'],
            'duree_apprentissage'=>$jsonPerUuid['duree_apprentissage'], 'langue_ressource'=>$jsonPerUuid['langues_ressource'],
            'mots_cles'=>$jsonPerUuid['mots_cles'],'association_associates'=>$jsonPerUuid['association_associates'],'specialites'=>$jsonPerUuid['specialites'],
            'vignette'=>$jsonPerUuid['vignette']
        ));
}}


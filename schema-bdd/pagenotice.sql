-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : dim. 20 mars 2022 à 10:50
-- Version du serveur : 10.6.7-MariaDB
-- Version de PHP : 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `wordpress`
--

-- --------------------------------------------------------

--
-- Structure de la table `pagenotice`
--

CREATE TABLE `pagenotice` (
  `uuid` varchar(36) NOT NULL,
  `vignette` text DEFAULT NULL,
  `titre` text NOT NULL,
  `description` text NOT NULL,
  `ressource_lien` text DEFAULT NULL,
  `url` text DEFAULT NULL,
  `fichiers_attaches` text DEFAULT NULL,
  `etablissement_porteur` text DEFAULT NULL,
  `date_creation` int(11) NOT NULL,
  `date_modification` date NOT NULL,
  `auteur` text DEFAULT NULL,
  `niveaux` text DEFAULT NULL,
  `objectifs_pedagogiques` text DEFAULT NULL,
  `types_pedagogiques` text DEFAULT NULL,
  `activites_induites` text DEFAULT NULL,
  `langues_utilisateur` text DEFAULT NULL,
  `proposition_utilisation` text DEFAULT NULL,
  `duree_apprentissage` text DEFAULT NULL,
  `langue_ressource` text DEFAULT NULL,
  `mots_cles` text DEFAULT NULL,
  `domaines` text DEFAULT NULL,
  `specialites` text DEFAULT NULL,
  `association_associates` text DEFAULT NULL,
  `date_publication` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `pagenotice`
--
ALTER TABLE `pagenotice`
  ADD PRIMARY KEY (`uuid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
